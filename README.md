# Softup

Softup is a software update tool designed for raspberry pi devices. Softup is a part of toolchain existent in **meta-slavic**, which lays in https://gitlab.com/slawomir.dur/meta-slavic

## Contents
1. Description
2. General Information On MMC
3. Software Update Process

## Description

Softup is a tool that works with packages.
A package is a tar.gz file containing boot files and rootfs.<br/>
Each package has its sha256 hash in its name. Thus, it makes it look like:<br/>
<br/>
NAME_HASH.tar.gz
<br/>
<br/>
Example:
<br/>
**package_123456789765432345678765432345678976543.tar.gz**
<br/>
<br/>
Softup works with specially prepared MMC devices that have a well-defined partition table.
The partition table is described below.

## General Information on MMC

### Partition table

The following partition table can be prepared by using **prepare_mmc.sh** from **meta-slavic** on a **32G** mmc device.

| Device| Size |Destiny|
|-------|------|---------|
|/dev/mmcblk0p1| 17M  |boot|
|/dev/mmcblk0p4| -    |extended|
|/dev/mmcblk0p5| 12G  |slot 1|
|/dev/mmcblk0p6| 12G  |slot 2|
|/dev/mmcblk0p7| 2G   |inventory|

#### Boot

Boot comprises the following files needed to boot raspberrypi:
1. Image - kernel image
2. kernel8.img - u-boot binary
3. bcm2711-rpi-4-b.dtb - device tree for raspberrypi4b device
4. cmdline.txt
5. config.txt - raspberrypi boot config txt file
6. fixup.dat files
7. start.elf files

#### Slot 1 and slot 2

Slots contain rootfs. Which slot is loaded is steered with cmdline.txt setting: root=DEVICE

#### Inventory

Inventory contains a json file with a description of currently active and inactive packages.
It also contains the actual package files.
<br/>
<br/>
The configuration **config.json** comprises two json objects, each defining a slot.
One of the slot is **active** and the other is **inactive**. 
The slot is described with a **name** and a **hash** of the loaded package.
If there is no currently loaded package on a slot, then this slot is described as **empty**
<br/>
<br/>
Example:<br/>
<pre>
{<br/>
    "running": NAME_HASH.tar.gz,<br/>
    "active": {<br/>
        "name": NAME,<br/>
        "hash": HASH,<br/>
        "package_name": NAME_HASH.tar.gz
    },<br/>
    "inactive": "empty"<br/>
}
</pre>
<br/>
<br/>
Also, inventory contains the packages themselves named by the convention described above.

## Software update process