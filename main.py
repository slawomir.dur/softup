#!/usr/bin/python3
import json
import os
import sys
import hashlib
import tarfile
import shutil
import cmd
import logging

from package.handler import PackageHandler
from package.package import Package

logging.basicConfig(filename='log.txt',
                    format='[%(levelname)s] - %(asctime)s - %(message)s',
                    level=logging.INFO)

logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))


def parse_args(args):
    return args.strip().split()


class Softup(cmd.Cmd):
    prompt = '(Softup) $ '
    intro = 'Welcome to Softup - Software Update Tool'
    doc_header = "The list of all commands that Softup" \
                 " supports\nTo learn more type 'help <command>'"
    package_handler = PackageHandler()

    def do_inventory(self, args):
        """
        Shows current inventory status
        """
        print(json.dumps(self.package_handler.get_inventory(), indent=4))

    def do_save(self, args):
        """
        Saves package to non-active slot in inventory
        USAGE:
        save PACKAGE_PATH
        """
        args = parse_args(args)
        package = Package(args[0])
        self.package_handler.save(package)

    def do_activate(self, args):
        """
        Activates package and deactivates currently active package
        """
        self.package_handler.activate()

    def do_apply(self, args):
        """
        Applies changes done during the session
        """
        self.package_handler.write()


def main():
    package_handler = PackageHandler()
    package = Package('/home/slavic-d/tarball_c7f171c6593749bb8a4cd92ab6f6c4dadfac07af12f0dbc530318bf169b64365.tar.gz')
    package_handler.save(package)
    package_handler.activate()
    package_handler.write()

    """package_name, package_hash = parse_package_name(package)
    validate_package_hash(package, package_hash)
    package_loc = unpack_package(package)
    validate_content(package_loc)
    """


if __name__ == '__main__':
    Softup().cmdloop()
