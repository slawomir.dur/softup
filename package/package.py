import hashlib
import logging
import os
import tarfile

from package import config, util


class Package:
    def __init__(self, path):
        self.path = path
        if not os.path.exists(self.path):
            raise RuntimeError(f'Package at {self.path} does not exist')
        self.package_name, self.package_hash = self.parse_package_name(self.path)
        self.validate_package_hash()
        self.validate_package_contents()

    @staticmethod
    def parse_package_name(package: str) -> tuple:
        package = os.path.basename(package)
        i = package.rfind('.tar.gz')
        if i == -1:
            raise RuntimeError(f'Package name not ending in .tar.gz')
        package = package[:i]
        i = package.rfind('_')
        package_name = package[:i]
        package_hash = package[i + 1:]
        return package_name, package_hash

    def validate_package_hash(self):
        real_package_hash = util.sha256(self.path)
        if real_package_hash != self.package_hash:
            raise RuntimeError(f'Package hash error: {self.package_hash} != {real_package_hash}')

    def validate_package_contents(self):
        util.wipe_tmpdir()
        util.unpack_tarball(self.path, config.SOFTUP_TMP)
        try:
            util.check_dir(f'{config.SOFTUP_TMP}/boot')
            util.check_dir(f'{config.SOFTUP_TMP}/root')
        except RuntimeError:
            logging.error('Package contents is invalid')
            raise
        util.wipe_tmpdir()

    def get_name(self) -> str:
        return self.package_name

    def get_hash(self) -> str:
        return self.package_hash

    def to_dict(self):
        return {
            'name': self.package_name,
            'hash': self.package_hash
        }

    @staticmethod
    def from_dict(dict_, root):
        basename = f'{dict_["name"]}_{dict_["hash"]}.tar.gz'
        path = os.path.join(root, basename)
        return Package(path)

