import json
import logging
import os

from package import util, config
from package.inventory import PackageInventory
from package.package import Package


def do_fallback_copy():
    logging.info('performing fallback copy in case sth goes awry')
    util.wipe_dir(config.FALLBACK_BOOT)
    util.wipe_dir(config.FALLBACK_ROOT)
    util.copy(f"{config.BOOT_MNT}/*", config.FALLBACK_BOOT)
    util.copy(f"{config.ROOT_MNT}/*", config.FALLBACK_ROOT)


def load_package_from_memory(path: str):
    if not os.path.exists(path):
        return None
    with open(path) as f:
        contents = f.read()
        if contents is None or contents == '':
            return None
        return Package.from_dict(json.loads(contents), config.SOFTUP_INVENTORY)


def fallback():
    logging.info('performing fallback')
    util.strict_umount_boot_root()
    util.mount_boot_root()
    util.copy(f"{config.FALLBACK_BOOT}/*", config.BOOT_MNT)
    util.copy(f"{config.FALLBACK_ROOT}/*", config.ROOT_MNT)
    util.umount(config.BOOT_DEV)
    util.umount(config.ROOT_DEV)


class PackageHandler:
    def __init__(self):
        util.create_workdir()
        self.packages = {}
        self.inventory = PackageInventory()
        self.load_current_state()

    def load_current_state(self):
        active = load_package_from_memory(f'{config.SOFTUP_INVENTORY}/{config.ACTIVE}')
        inactive = load_package_from_memory(f'{config.SOFTUP_INVENTORY}/{config.INACTIVE}')
        self._save_active(active)
        self._save_not_active(inactive)

    def get_not_active(self) -> Package:
        return self.packages.get(False)

    def get_active(self) -> Package:
        return self.packages.get(True)

    def _save_not_active(self, package: Package):
        self.packages[False] = package

    def _save_active(self, package: Package):
        self.packages[True] = package

    def activate(self):
        tmp = self.get_active()
        self._save_active(self.get_not_active())
        self._save_not_active(tmp)

    def save(self, package: Package):
        self.inventory.remove(self.get_not_active())
        self._save_not_active(package)
        self.inventory.add(self.get_not_active())

    def write(self):
        self.save_inventory()
        self.perform_update()

    def save_inventory(self):
        logging.info('writing in-memory inventory state to memory')
        for key in self.packages:
            slot_name = config.ACTIVE if key else config.INACTIVE
            with open(f'{config.SOFTUP_INVENTORY}/{slot_name}', 'w+') as f:
                package = self.packages[key]
                if package is not None:
                    json.dump(package.to_dict(), f)
        logging.info('writing in-memory inventory OK')

    def perform_update(self):
        logging.info('updating software on eMMC')
        active = self.get_active()
        util.silent_umount_boot_root()
        util.mount_boot_root()
        try:
            do_fallback_copy()
            util.wipe_tmpdir()
            util.unpack_tarball(active.path, config.SOFTUP_TMP)
            util.wipe_dir(config.BOOT_MNT)
            util.copy(f'{config.SOFTUP_TMP}/boot/*', config.BOOT_MNT)
            util.wipe_dir(config.ROOT_MNT)
            util.copy(f'{config.SOFTUP_TMP}/root/*', config.ROOT_MNT)
            util.strict_umount_boot_root()
            logging.info('updating software OK')
        except Exception as e:
            logging.error(f'error updating software: {e}')
            fallback()

    def get_inventory(self):
        def get_not_null(obj):
            if obj is not None:
                return obj.to_dict()
            return config.EMPTY
        return {
            config.ACTIVE: get_not_null(self.get_active()),
            config.INACTIVE: get_not_null(self.get_not_active())
        }
