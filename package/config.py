import enum


BOOT = 'boot'
ROOT = 'root'

SOFTUP_DIR = '/tmp/softup'
SOFTUP_INVENTORY = f'{SOFTUP_DIR}/inventory'
SOFTUP_TMP = f'{SOFTUP_DIR}/tmp'

SOFTUP_FALLBACK = f'{SOFTUP_DIR}/fallback'
FALLBACK_BOOT = f'{SOFTUP_FALLBACK}/{BOOT}'
FALLBACK_ROOT = f'{SOFTUP_FALLBACK}/{ROOT}'

ACTIVE = 'active'
INACTIVE = 'inactive'
EMPTY = '<empty>'

BOOT_DEV = "/dev/mmcblk0p1"
ROOT_DEV = "/dev/mmcblk0p5"

BOOT_MNT = f"/mnt/{BOOT}"
ROOT_MNT = f"/mnt/{ROOT}"

# ==============================


class Devices(str, enum.Enum):
    BOOT = '/dev/mmcblk0p1'
    SLOT_1 = '/dev/mmcblk0p5'
    SLOT_2 = '/dev/mmcblk0p6'
    CONFIGURATION = '/dev/mmcblk0p7'

