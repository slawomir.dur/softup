import os
import shutil

from package.config import SOFTUP_INVENTORY
from package.package import Package


class PackageInventory:
    def __init__(self):
        self.path = SOFTUP_INVENTORY

    def add(self, package: Package):
        if package is None:
            return
        shutil.copy(package.path, f'{self.path}/{os.path.basename(package.path)}')

    def remove(self, package: Package):
        if package is None:
            return
        os.remove(f'{self.path}/{os.path.basename(package.path)}')
