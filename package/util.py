import hashlib
import os
import logging
import shutil
import tarfile

from package import config


def create_workdir():
    logging.info('creating workdir')
    if not os.path.exists(config.SOFTUP_DIR):
        os.mkdir(config.SOFTUP_DIR)
        os.mkdir(config.SOFTUP_TMP)
        os.mkdir(config.SOFTUP_INVENTORY)
    if not os.path.exists(config.SOFTUP_TMP):
        logging.warning(f'{config.SOFTUP_TMP} non-existent while {config.SOFTUP_DIR} existed. Recreating...')
        os.mkdir(config.SOFTUP_TMP)
    if not os.path.exists(config.SOFTUP_INVENTORY):
        logging.warning(f'{config.SOFTUP_INVENTORY} non-existent while {config.SOFTUP_DIR} existed. Recreating...')
        os.mkdir(config.SOFTUP_INVENTORY)
    if not os.path.exists(config.SOFTUP_FALLBACK):
        os.mkdir(config.SOFTUP_FALLBACK)
    wipe_tmpdir()
    logging.info('Workdir in OK state')


def wipe_dir(directory, recursive=True):
    logging.info(f'wiping contents of {directory}')
    for root, dirs, files in os.walk(directory):
        for dir_ in dirs:
            shutil.rmtree(os.path.join(root, dir_))
        for file in files:
            os.remove(os.path.join(root, file))
        if not recursive:
            break


def wipe_tmpdir():
    wipe_dir(config.SOFTUP_TMP)


def check_dir(directory):
    logging.info(f'checking directory {directory}')
    if not os.path.exists(directory):
        raise RuntimeError(f'Directory {directory} does not exist')


def unpack_tarball(path, dest):
    logging.info(f'unpacking tarball {path} to {dest}')
    if not tarfile.is_tarfile(path):
        raise RuntimeError(f'{path} not a tarball')
    tar = tarfile.open(path, 'r:gz')
    tar.extractall(dest)
    tar.close()


def syscall(cmd: str):
    ret = os.system(cmd)
    if ret != 0:
        raise RuntimeError(f'Failed to exec: {cmd} with exit code {ret}')


def copy(src: str, dst: str):
    logging.info(f'copying {src} to {dst}')
    syscall(f"mkdir -p {dst}")
    syscall(f"cp -r {src} {dst}")


def mount(device: str, mount_point: str):
    logging.info(f'mounting {device} on {mount_point}')
    syscall(f'mount {device} {mount_point}')


def umount(device: str):
    logging.info(f'umounting {device}')
    syscall(f'umount {device}')


def silent_umount(device: str):
    try:
        umount(device)
    except RuntimeError:
        pass


def silent_umount_boot():
    silent_umount(config.BOOT_DEV)


def silent_umount_root():
    silent_umount(config.ROOT_DEV)


def silent_umount_boot_root():
    silent_umount_boot()
    silent_umount_root()


def strict_umount_boot():
    umount(config.BOOT_DEV)


def strict_umount_root():
    umount(config.ROOT_DEV)


def strict_umount_boot_root():
    strict_umount_boot()
    strict_umount_root()


def mount_boot_root():
    mount(config.BOOT_DEV, config.BOOT_MNT)
    mount(config.ROOT_DEV, config.ROOT_MNT)


def read_file(path: str, binary: bool = False):
    if not binary:
        with open(path) as file:
            return file.read()
    with open(path, 'rb') as file:
        return file.read()


def sha256(path: str) -> str:
    file = read_file(path, binary=True)
    return hashlib.sha256(file).hexdigest()
